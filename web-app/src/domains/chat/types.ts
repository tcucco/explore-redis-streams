interface ChatMessage {
  readonly content: string;
  readonly id: number;
  readonly senderName: string;
  readonly timestamp: string;
}

export {
  ChatMessage,
};
