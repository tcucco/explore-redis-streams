import * as actionTypes from './action-types';
import { ChatMessage } from './types';
import { Action } from '../../core/actions';

const setUserName = (name: string): Action<actionTypes.SetUserNamePayload> => ({
  type: actionTypes.SET_USER_NAME,
  payload: {
    name,
  },
});

const setChannelName = (name: string): Action<actionTypes.SetChannelNamePayload> => ({
  type: actionTypes.SET_CHANNEL_NAME,
  payload: {
    name,
  },
});

const setMessage = (message: string): Action<actionTypes.SetMessagePayload> => ({
  type: actionTypes.SET_MESSAGE,
  payload: {
    message,
  },
});

const setMessages = (messages: ReadonlyArray<ChatMessage>): Action<actionTypes.SetMessagesPayload> => ({
  type: actionTypes.SET_MESSAGES,
  payload: {
    messages,
  },
});

const receiveMessage = (message: ChatMessage): Action<actionTypes.ReceiveMessagePayload> => ({
  type: actionTypes.RECEIVE_MESSAGE,
  payload: {
    message,
  },
});

export {
  receiveMessage,
  setChannelName,
  setMessage,
  setMessages,
  setUserName,
};
