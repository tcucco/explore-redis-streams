import * as React from 'react';
import { ChatLogItem } from '../chat-log-item';
import { ChatMessage } from '../../types';

interface Props {
  messages: Array<ChatMessage>;
  onLoad: () => void;
  onUnload: () => void;
}

class ChatLog extends React.PureComponent<Props> {
  componentDidMount() {
    this.props.onLoad();
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  renderMessages() {
    const {
      messages,
    } = this.props;

    return messages.map(message => (
      <ChatLogItem
        key={message.id}
        message={message.content}
        senderName={message.senderName}
        timestamp={message.timestamp}
      />
    ));
  }

  render() {
    return (
      <div>
        {this.renderMessages()}
      </div>
    );
  }
}

export {
  ChatLog,
};
