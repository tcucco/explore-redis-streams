import { ChatLog as Component } from './component';
import { connect } from './connect';

const ChatLog = connect(Component);

export {
  ChatLog,
};
