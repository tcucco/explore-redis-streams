import { connect as connectRedux } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import * as chatSelectors from '../../selectors';
import {
  obtainWebSocket,
  releaseWebSocket,
  sendMessage,
} from '../../../../core/awaits';
import { receiveMessage } from '../../actions';
import { WSExtra } from '../../../../core/store';

const host = (document as any).location.host as string;
const url = `ws://${host}/ws`;
const getId = (() => {
  let id = 1;
  return () => id++;
})();

const handleMessage = (e: MessageEvent, store: WSExtra) => {
  store.dispatch(receiveMessage({
    content: e.data,
    id: getId(),
    senderName: 'Unknown',
    timestamp: (new Date()).toISOString(),
  }));
};

const mapState = createStructuredSelector({
  messages: chatSelectors.messagesSelector(),
});

const mapDispatch = {
  onLoad: () => obtainWebSocket(url, handleMessage),
  onUnload: () => releaseWebSocket(url, handleMessage)
};

const connect = connectRedux(mapState, mapDispatch);

export {
  connect,
};
