import * as React from 'react';
import {
  Flex,
  Button,
  TextInput,
} from '../../../../core/components';

interface Props {
  message: string | null;
  onChange: (value: string) => void;
  onLoad: () => void;
  onUnload: () => void;
  onSendMessage: (message: string) => void;
}

class ChatInput extends React.PureComponent<Props> {
  componentDidMount() {
    this.props.onLoad();
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  onChange = (name: string, value: string): void => {
    this.props.onChange(value);
  }

  onSendMessage = (e: any): void => {
    const {
      message,
      onChange,
      onSendMessage,
    } = this.props;
    if (e) {
      e.preventDefault();
    }
    if (!message) {
      return;
    }
    onSendMessage(message);
    onChange('');
  }

  render() {
    return (
      <form onSubmit={this.onSendMessage}>
        <Flex.Row>
          <Flex.RowItem width={1}>
            <TextInput
              name="message"
              onChange={this.onChange}
              value={this.props.message || ''}
            />
          </Flex.RowItem>
          <Flex.RowItem width="auto">
            <Button type="submit" onClick={this.onSendMessage}>
              Send
            </Button>
          </Flex.RowItem>
        </Flex.Row>
      </form>
    );
  }
}

export {
  ChatInput,
};
