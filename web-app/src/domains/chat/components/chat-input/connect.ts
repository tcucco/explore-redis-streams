import { connect as connectRedux } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import * as chatSelectors from '../../selectors';
import * as chatActions from '../../actions';
import {
  obtainWebSocket,
  releaseWebSocket,
  sendMessage,
} from '../../../../core/awaits';

const host = (document as any).location.host as string;
const url = `ws://${host}/ws`;

const mapState = createStructuredSelector({
  message: chatSelectors.messageSelector(),
});

const mapDispatch = {
  onChange: chatActions.setMessage,
  onLoad: () => obtainWebSocket(url),
  onUnload: () => releaseWebSocket(url),
  onSendMessage: (message: string) => sendMessage(url, message),
};

const connect = connectRedux(mapState, mapDispatch);

export {
  connect,
};
