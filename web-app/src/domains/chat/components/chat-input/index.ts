import { ChatInput as Component } from './component';
import { connect } from './connect';

const ChatInput = connect(Component);

export {
  ChatInput,
}
