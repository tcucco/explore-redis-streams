import injectSheet from 'react-jss';

const styles = {
  '@global': {
    'html,body': {
      fontFamily: 'Courier New',
    },
    '*': {
      boxSizing: 'border-box',
    },
  },
  container: {
    position: 'absolute',
    right: 0,
    top: 0,
    left: 0,
    bottom: 0,
    padding: 16,
  },
};

// TODO: have to cast this to any??
const style = injectSheet(styles as any);

export {
  style,
};
