import { Chat as Component } from './component';
import { connect } from './connect';
import { style } from './style';

const Chat = connect(style(Component));

export {
  Chat,
};
