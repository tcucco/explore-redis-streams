import * as React from 'react';
import { ChatInput } from '../chat-input';
import { ChatLog } from '../chat-log';
import { Flex } from '../../../../core/components';

interface Props {
  classes: any,
}

class Chat extends React.PureComponent<Props> {
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.container}>
        <Flex.Col>
          <Flex.ColItem height="auto">
            <ChatInput />
          </Flex.ColItem>
          <Flex.ColItem height={1} overflow="scroll">
            <ChatLog />
          </Flex.ColItem>
        </Flex.Col>
      </div>
    );
  }
}

export {
  Chat,
};
