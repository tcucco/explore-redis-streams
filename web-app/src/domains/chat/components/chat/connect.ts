import { connect as connectRedux } from 'react-redux';

const connect = connectRedux();

export {
  connect,
};
