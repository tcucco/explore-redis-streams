import { ChatLogItem as Component } from './component';
import { connect } from './connect';

const ChatLogItem = connect(Component);

export {
  ChatLogItem,
};
