import * as React from 'react';

interface Props {
  message: string;
  senderName: string;
  timestamp: string;
}

class ChatLogItem extends React.PureComponent<Props> {
  render() {
    const {
      message,
      senderName,
      timestamp,
    } = this.props;

    return (
      <div>{senderName} @ {timestamp}: {message}</div>
    );
  }
}

export {
  ChatLogItem,
};
