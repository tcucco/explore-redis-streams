import { State } from '../../core/store';
import { Selector } from '../../core/selectors';
import { ChatState } from './reducer';
import { createSelector } from 'reselect';

const chatSelector = (state: State) => state.chat;

const userNameSelector = () => createSelector(
  chatSelector,
  chat => chat.userName,
);

const channelNameSelector = () => createSelector(
  chatSelector,
  chat => chat.channelName,
);

const messageSelector = () => createSelector(
  chatSelector,
  chat => chat.message,
);

const messagesSelector = () => createSelector(
  chatSelector,
  chat => chat.messages.slice().reverse(),
);

export {
  userNameSelector,
  channelNameSelector,
  messageSelector,
  messagesSelector,
};
