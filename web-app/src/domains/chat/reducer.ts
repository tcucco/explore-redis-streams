import { ChatMessage } from './types';
import { Action } from '../../core/actions';
import { createStructuredReducer } from '../../core/reducers';
import * as actionTypes from './action-types';

interface ChatState {
  readonly userName: string | null;
  readonly channelName: string | null;
  readonly message: string | null;
  readonly messages: ReadonlyArray<ChatMessage>;
}

const initialState: ChatState = {
  userName: null,
  channelName: null,
  message: null,
  messages: [],
};

const reducer = createStructuredReducer<ChatState>(
  initialState,
  {
    [actionTypes.SET_USER_NAME]:
      (state: ChatState, action: Action<actionTypes.SetUserNamePayload>) =>
        ({ ...state, name: action.payload.name }),
    [actionTypes.SET_CHANNEL_NAME]:
      (state: ChatState, action: Action<actionTypes.SetChannelNamePayload>) =>
        ({ ...state, channelName: action.payload.name }),
    [actionTypes.SET_MESSAGE]:
      (state: ChatState, action: Action<actionTypes.SetMessagePayload>) =>
        ({ ...state, message: action.payload.message }),
    [actionTypes.SET_MESSAGES]:
      (state: ChatState, action: Action<actionTypes.SetMessagesPayload>) =>
        ({ ...state, messages: [...state.messages, ...action.payload.messages] }),
    [actionTypes.RECEIVE_MESSAGE]:
      (state: ChatState, action: Action<actionTypes.ReceiveMessagePayload>) =>
        ({ ...state, messages: [...state.messages, action.payload.message] }),
  },
);

export {
  ChatState,
  reducer,
};
