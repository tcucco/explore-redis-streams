import { ChatMessage } from './types';

const SET_USER_NAME = '@chat/SET_USER_NAME';
const SET_CHANNEL_NAME = '@chat/SET_CHANNEL_NAME';
const SET_MESSAGE = '@chat/SET_MESSAGE';
const SET_MESSAGES = '@chat/SET_MESSAGES';
const RECEIVE_MESSAGE = '@chat/RECEIVE_MESSAGE';

interface SetUserNamePayload {
  name: string,
}

interface SetChannelNamePayload {
  name: string,
}

interface SetMessagePayload {
  message: string,
}

interface SetMessagesPayload {
  messages: ReadonlyArray<ChatMessage>
}

interface ReceiveMessagePayload {
  message: ChatMessage,
}

export {
  SET_USER_NAME,
  SET_CHANNEL_NAME,
  SET_MESSAGE,
  SET_MESSAGES,
  RECEIVE_MESSAGE,
  SetUserNamePayload,
  SetChannelNamePayload,
  SetMessagePayload,
  SetMessagesPayload,
  ReceiveMessagePayload,
};
