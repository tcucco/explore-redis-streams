import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Chat } from './domains/chat/components/chat';
import { createStore } from './core/store';
import { Provider } from 'react-redux';

const store = createStore();

ReactDOM.render(
  (
    <Provider store={store}>
      <Chat />
    </Provider>
  ),
  document.getElementById('root'),
);
