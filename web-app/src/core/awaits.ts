import { ThunkArg, Dispatch, GetState } from './store';
import { MessageHandler } from './web-socket-cache';

const obtainWebSocket =
  (url: string, handler: MessageHandler | null = null) =>
    (dispatch: Dispatch, getState: GetState, ctx: ThunkArg) => {
      const entry = ctx.ws.obtain(url);
      if (handler !== null) {
        entry.onMessage(handler);
      }
    }

const releaseWebSocket =
  (url: string, handler: MessageHandler | null = null) =>
    (dispatch: Dispatch, getState: GetState, ctx: ThunkArg) => {
      if (handler !== null) {
        const entry = ctx.ws.get(url)
        entry.offMessage(handler);
      }
      ctx.ws.release(url);
    }

const sendMessage =
  (url: string, message: string) =>
    (dispatch: Dispatch, getState: GetState, ctx: ThunkArg) =>
      ctx.ws.send(url, message);

export {
  obtainWebSocket,
  releaseWebSocket,
  sendMessage,
};
