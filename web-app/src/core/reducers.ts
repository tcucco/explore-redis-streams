import { Action } from './actions';

interface ActionHandler<T> {
  (state: T, action: Action<any>): T,
}

interface Reducer<T> {
  (state: T, action: Action<any>): T,
}

function createStructuredReducer<T>(
  initialState: T,
  handlers: { [actionType: string]: ActionHandler<T> },
): Reducer<T> {
  return (state: T = initialState, action: Action<any>) => {
    if (action.type in handlers) {
      return handlers[action.type](state, action);
    }
    return state;
  };
}

export {
  createStructuredReducer,
};
