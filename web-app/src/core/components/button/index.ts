import { Button as Component } from './component';
import { style } from './style';

const Button = style(Component);

export {
  Button,
};
