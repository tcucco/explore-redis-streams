import injectSheet from 'react-jss';

const styles = {
  base: {
    border: '1px solid #888',
    borderRadius: 3,
    height: 30.4,
    backgroundColor: 'white',
  },
};

const style = injectSheet(styles as any);

export {
  style,
};
