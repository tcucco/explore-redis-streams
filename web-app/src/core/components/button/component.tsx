import * as React from 'react';

type ButtonType = 'button' | 'submit';

interface Props {
  children: React.ReactNode;
  classes: any;
  onClick: (value?: string) => void;
  type?: ButtonType;
  value?: string;
}

class Button extends React.PureComponent<Props> {
  onClick = (e: React.MouseEvent<HTMLElement>) => {
    const {
      onClick,
      value,
    } = this.props;
    e.preventDefault();
    onClick(value);
  }

  render() {
    const {
      children,
      classes,
      type,
    } = this.props;

    return (
      <button
        className={classes.base}
        onClick={this.onClick}
        type={type}
      >
        {children}
      </button>
    );
  }
}

export {
  Button,
};
