import { TextInput as Component } from './component';
import { style } from './style';

const TextInput = style(Component);

export {
  TextInput,
};
