import injectSheet from 'react-jss';

const styles = {
  base: {
    border: '1px solid #888',
    padding: 5,
    width: '100%',
    borderRadius: 3,
  },
};

const style = injectSheet(styles);

export {
  style,
};
