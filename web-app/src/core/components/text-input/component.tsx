import * as React from 'react';

interface Props {
  classes: any;
  name: string;
  onChange: (name: string, value: string) => void;
  placeholder?: string;
  value: string;
}

class TextInput extends React.PureComponent<Props> {
  onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const {
      onChange,
      name,
    } = this.props;

    onChange(name, e.target.value);
  }

  render() {
    const {
      classes,
      name,
      placeholder,
      value,
    } = this.props;

    return (
      <input
        className={classes.base}
        name={name}
        onChange={this.onChange}
        placeholder={placeholder}
        type="text"
        value={value}
      />
    )
  }
}

export {
  TextInput,
};
