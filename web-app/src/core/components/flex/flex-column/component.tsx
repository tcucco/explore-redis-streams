import * as React from 'react';

interface Props {
  classes: any;
  children: any;
}

const FlexColumn: React.SFC<Props> = ({ classes, children }) => (
  <div className={classes.base}>
    {children}
  </div>
);

export {
  FlexColumn,
};
