import injectSheet from 'react-jss';

const styles = {
  base: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
  },
};

const style = injectSheet(styles as any);

export {
  style,
};
