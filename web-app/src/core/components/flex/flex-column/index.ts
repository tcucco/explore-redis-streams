import { FlexColumn as Component } from './component';
import { style } from './style';

const FlexColumn = style(Component);

export {
  FlexColumn,
};
