type Missing = null | undefined;
type SizeProp = 'width' | 'height';
type SizeType = string | number | Missing;
type OverflowType = 'visible' | 'hidden' | 'scroll' | 'auto' | 'initial';

export {
  Missing,
  SizeProp,
  SizeType,
  OverflowType,
};
