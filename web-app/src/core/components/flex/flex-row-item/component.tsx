import * as React from 'react';
import { SizeType, OverflowType, Missing } from '../types';
import { getItemStyle } from '../utils';

interface Props {
  width?: SizeType;
  overflow?: OverflowType | Missing;
  children: any;
}

const FlexRowItem: React.SFC<Props> = ({ width, children, overflow }) => (
  <div style={getItemStyle('width', width, overflow)}>
    {children}
  </div>
);

export {
  FlexRowItem,
};

