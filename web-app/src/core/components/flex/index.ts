import { FlexColumn } from './flex-column';
import { FlexRow } from './flex-row';
import { FlexColumnItem } from './flex-column-item';
import { FlexRowItem } from './flex-row-item';

const Flex = {
  Col: FlexColumn,
  Row: FlexRow,
  ColItem: FlexColumnItem,
  RowItem: FlexRowItem,
};

export {
  Flex,
};
