import * as React from 'react';

interface Props {
  classes: any;
  children: any;
}

const FlexRow: React.SFC<Props> = ({ classes, children }) => (
  <div className={classes.base}>
    {children}
  </div>
);

export {
  FlexRow,
};
