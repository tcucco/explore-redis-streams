import { FlexRow as Component } from './component';
import { style } from './style';

const FlexRow = style(Component);

export {
  FlexRow,
};
