import * as React from 'react';
import { SizeType, OverflowType, Missing } from '../types';
import { getItemStyle } from '../utils';

interface Props {
  children: any;
  height?: SizeType;
  overflow?: OverflowType | Missing;
}

const FlexColumnItem: React.SFC<Props> = ({ height, children, overflow }) => (
  <div style={getItemStyle('height', height, overflow)}>
    {children}
  </div>
);

export {
  FlexColumnItem,
};
