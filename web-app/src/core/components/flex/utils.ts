import { SizeProp, SizeType, OverflowType, Missing } from './types';

interface FlexItemStyle {
  width?: string;
  height?: string;
  flex?: number;
  overflowX?: OverflowType;
  overflowY?: OverflowType;
}

const getItemStyle = (sizeProp: SizeProp, size: SizeType, overflow: OverflowType | Missing): FlexItemStyle => {
  const overflowProp = sizeProp === 'width' ? 'overflowX' : 'overflowY';
  const overflowValue = overflow || 'initial';
  if (!size) {
    return { [sizeProp]: 'auto', [overflowProp]: overflowValue }
  }
  if (typeof size === 'string') {
    return { [sizeProp]: size, [overflowProp]: overflowValue };
  }
  return { flex: size, [overflowProp]: overflowValue };
}

export {
  getItemStyle,
};
