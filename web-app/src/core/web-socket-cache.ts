interface MessageHandler {
  (messageEvent: MessageEvent, extra: any): void
}

class WebSocketCacheEntry<T> {
  count: number;
  extra: T;
  handlers: Array<MessageHandler>;
  url: string;
  ws: WebSocket | null;

  constructor(url: string, extra: T) {
    this.count = 0;
    this.extra = extra;
    this.handlers = [];
    this.url = url;
    this.ws = null;
  }

  isAlive() {
    return this.ws !== null && this.ws.readyState === 1;
  }

  setup() {
    if (this.ws !== null) {
      throw new Error('WebSocket is already open');
    }
    if (this.count === 0) {
      throw new Error('WebSocket cannot be opened when refcount is 0');
    }

    this.ws = new WebSocket(this.url);
    this.ws.onmessage = this.handleMessage;
    this.ws.onclose = this.handleClose;
  }

  teardown() {
    if (this.isAlive()) {
      (this.ws as WebSocket).close();
    }
    this.ws = null;
  }

  obtain() : WebSocketCacheEntry<T> {
    this.count += 1;
    if (this.count === 1) {
      this.setup();
    }
    return this;
  }

  release(): void {
    if (this.count === 0) {
      return;
    }

    this.count -= 1;

    if (this.count === 0) {
      this.teardown();
    }
  }

  send(message: string): void {
    if (!this.isAlive()) {
      this.teardown();
      this.setup();
      (this.ws as WebSocket).onopen = () => this.sendAfterOpen(message);
    } else {
      (this.ws as WebSocket).send(message);
    }
  }

  sendAfterOpen(message: string): void {
    this.send(message);
    (this.ws as WebSocket).onopen = null;
  }

  onMessage(handler: MessageHandler): void {
    this.handlers = this.handlers.concat(handler);
  }

  offMessage(handler: MessageHandler): void {
    this.handlers = this.handlers.filter(cb => cb !== handler);
  }

  handleMessage = (event: MessageEvent): void => this.handlers.forEach(cb => cb(event, this.extra))

  handleClose = (): void => {
    console.log('closed')
    this.ws = null;
  }
}

class WebSocketCache<T> {
  extra: T;
  webSockets: { [url: string]: WebSocketCacheEntry<T> };

  constructor(extra: T) {
    this.webSockets = {};
    this.extra = extra;
  }

  has(url: string): boolean {
    return url in this.webSockets;
  }

  get(url: string) : WebSocketCacheEntry<T> {
    if (!this.has(url)) {
      throw new Error('WebSocketCacheEntry not found');
    }
    return this.webSockets[url];
  }

  obtain(url: string): WebSocketCacheEntry<T> {
    if (!this.has(url)) {
      this.webSockets[url] = new WebSocketCacheEntry(url, this.extra);
    }
    return this.webSockets[url].obtain();
  }

  release(url: string) : boolean {
    if (!this.has(url)) {
      return false;
    }
    this.webSockets[url].release();
    return true;
  }

  send(url: string, message: string): void {
    this.get(url).send(message);
  }
}

export {
  MessageHandler,
  WebSocketCache,
}
