import {
  Dispatch,
  Store,
  applyMiddleware,
  combineReducers,
  compose,
  createStore as createReduxStore,
} from 'redux';
import thunk from 'redux-thunk';
import { reducer as chatReducer, ChatState } from '../domains/chat/reducer';
import { WebSocketCache } from './web-socket-cache';

interface State {
  readonly chat: ChatState;
}

interface GetState {
  (): State,
}

interface WSExtra {
  dispatch: Dispatch;
  getState: GetState;
};

interface ThunkArg {
  readonly ws: WebSocketCache<WSExtra>,
}

const createStore = (): Store<State> => {
  const isProduction = process.env.NODE_ENV === 'production';

  const w: any = window as any;
  const devTools = !isProduction && w.devToolsExtension ? w.devToolsExtension() : (f:any) => f;

  let store : Store;
  const reducer = combineReducers<State>({
    chat: chatReducer,
  });
  const wsExtra = {
    dispatch: (action: any) => store.dispatch(action),
    getState: () => store.getState(),
  };
  const thunkArg: ThunkArg = {
    ws: new WebSocketCache(wsExtra),
  };

  store = createReduxStore(
    reducer,
    {},
    compose(
      applyMiddleware(thunk.withExtraArgument(thunkArg)),
      devTools,
    ),
  );

  return store;
};

export {
  Dispatch,
  GetState,
  State,
  WSExtra,
  ThunkArg,
  createStore,
};
