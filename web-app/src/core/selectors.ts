import { State } from './store';

interface Selector<T> {
  (state: State, props: object): T,
}

export {
  Selector,
}
