This project is a simple multi-user chat program. I began it as an opportunity to explore some
technologies and concepts:

- Redis streams
- Python 3.7 asyncio changes
- TypeScript
- JSS
- WebSockets (how to use them well with React and Redux)
- Python Dependency Injection
