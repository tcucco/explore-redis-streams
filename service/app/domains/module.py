import os
import aioredis
from app.core.dependency_injector import DependencyInjectorDefinition
import app.domains.chat.module as chat_module

def boot_container(di: DependencyInjectorDefinition) -> DependencyInjectorDefinition:
  di.add(
    'redis_read',
    lambda _: aioredis.create_connection(os.getenv('REDIS_CONNECTION')),
  )
  di.add(
    'redis_write',
    lambda _: aioredis.create_connection(os.getenv('REDIS_CONNECTION')),
  )
  chat_module.boot_container(di)
  return di
