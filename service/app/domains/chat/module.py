from app.core.dependency_injector import DependencyInjectorDefinition
from app.domains.chat import interactions as I

def boot_container(di: DependencyInjectorDefinition) -> DependencyInjectorDefinition:
  di.add(
    'chat_interaction',
    lambda c: I.ChatInteraction(c.get('redis_write')),
  )
  return di
