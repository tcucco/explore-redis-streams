'''Interaction class for chatting via redis streams.'''
from typing import (Optional)
from dataclasses import dataclass
from aioredis import RedisConnection
from app.core.data import redis_connection
from app.core.interactor_base import InteractorBase

STREAM_NAME = 'chat'
ACK = False

@dataclass
class State:
  interactor: InteractorBase
  timeout: int
  conn: RedisConnection
  last_message_id: str = '$'

class ChatInteraction:
  '''Interaction for chatting via redis streams.'''
  def __init__(self, redis: RedisConnection) -> None:
    self.redis: RedisConnection = redis

  async def send_messages(self, state: State, streams: list) -> str:
    for (_, messages) in streams:
      for (message_id, props) in messages:
        last_message_id = message_id
        bmessage = dict(zip(props[::2], props[1::2]))
        message = {k.decode(): v.decode() for (k, v) in bmessage.items()}
        await state.interactor.send_json(message)
    return last_message_id

  async def send_message(self, state: State) -> Optional[str]:
    '''Check for new messages in the stream and send them over the Interactor if found.'''
    streams = await state.conn.execute(
      'XREAD',
      'BLOCK',
      state.timeout,
      'STREAMS',
      STREAM_NAME,
      state.last_message_id,
    )
    if streams:
      last_id = await self.send_messages(state, streams)
      state.last_message_id = last_id
      return last_id
    return None

  async def receive_message(self, state: State) -> Optional[str]:
    message = await state.interactor.receive(timeout=state.timeout)
    if not message:
      return None
    await state.conn.execute('XADD', STREAM_NAME, '*', 'message', message)
    if ACK:
      await state.interactor.send_text('received {}'.format(message))
    return message

  async def get_latest_id(self, state: State) -> str:
    messages = await state.conn.execute('XREVRANGE', STREAM_NAME, '+', '-', 'COUNT', 1)
    if messages:
      (message_id, _) = messages[0]
      return message_id
    return '0'

  async def execute(self, interactor: InteractorBase, timeout: int):
    async with redis_connection(self.redis) as conn:
      state = State(interactor, timeout, conn)
      state.last_message_id = await self.get_latest_id(state)

      while not state.interactor.closed():
        await self.send_message(state)
        await self.receive_message(state)
