"""Defines dependency injection collections."""
from typing import (Any, Callable, MutableMapping, Sequence)

DependencyFactory = Callable[['DependencyInjector'], Any]

def service(dependencies: Sequence[str], factory: Callable) -> DependencyFactory:
  """Returns a service factory that does simple location injection."""
  def build(di: 'DependencyInjector') -> Any:
    """Build a dependency from a list of keys."""
    return factory(*[di.get(key) for key in dependencies])
  return build

class DependencyInjectorDefinition:
  """Maps dependency keys to factories."""
  def __init__(self, environment: str, *, allow_overwrite: bool = False) -> None:
    self.environment: str = environment
    self.allow_overwrite: bool = allow_overwrite
    self.services: MutableMapping[str, DependencyFactory] = {}

  def add(self, key: str, factory: DependencyFactory) -> None:
    if not self.allow_overwrite:
      if key in self.services:
        raise KeyError('Key {0} already added to container'.format(key))
    self.services[key] = factory

  def add_service(self, key: str, dependencies: Sequence[str], factory: Callable) -> None:
    self.add(key, service(dependencies, factory))

  def get(self, key: str) -> DependencyFactory:
    return self.services[key]

  def get_injector(self) -> 'DependencyInjector':
    return DependencyInjector(self)

class DependencyInjector:
  def __init__(self, dependency_injector_definition: DependencyInjectorDefinition) -> None:
    self.dependency_injector_definition = dependency_injector_definition
    self.services: MutableMapping[str, Any] = {}

  def get(self, key: str) -> Any:
    if key not in self.services:
      self.services[key] = self.dependency_injector_definition.get(key)(self)
    return self.services[key]
