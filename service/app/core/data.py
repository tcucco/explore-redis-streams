'''Data methods.'''
from contextlib import asynccontextmanager
from typing import AsyncIterator
from aioredis import RedisConnection

@asynccontextmanager
async def redis_connection(redis: RedisConnection) -> AsyncIterator[RedisConnection]:
  conn = await redis
  try:
    yield conn
  finally:
    conn.close()
    await conn.wait_closed()
