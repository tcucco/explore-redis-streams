'''Domain wrapper for interactor objects.'''
import abc
from typing import (Any, Optional)

class InteractorBase:
  '''Abstract base class for presentation interactors.'''
  __metaclass__ = abc.ABCMeta

  @abc.abstractmethod
  async def send_json(self, message: Any) -> None:
    '''send json message'''

  @abc.abstractmethod
  async def send_text(self, message: str) -> None:
    '''send text message'''

  @abc.abstractmethod
  async def receive(self, *, timeout: int = 0) -> Optional[str]:
    '''receive data'''

  @abc.abstractmethod
  def closed(self) -> bool:
    '''whether or not the interactor is closed'''
