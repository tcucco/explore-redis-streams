'''WebSocket implementation of an Interactor.'''
import asyncio
from typing import (Any, Optional)
from aiohttp import (WSMsgType)
from aiohttp.web import (WebSocketResponse)
from app.core.interactor_base import InteractorBase

class WebSocketInteractor(InteractorBase):
  '''WebSocket implementation of an Interactor.'''
  def __init__(self, wsock: WebSocketResponse):
    self.wsock = wsock

  async def send_json(self, message: Any) -> None:
    await self.wsock.send_json(message)

  async def send_text(self, message: str) -> None:
    await self.wsock.send_str(message)

  async def receive(self, *, timeout: int = 0) -> Optional[str]:
    try:
      message = await self.wsock.receive(timeout=timeout / 1000)
    except asyncio.TimeoutError:
      return None
    if message.type == WSMsgType.TEXT:
      return message.data
    return None

  def closed(self):
    return self.wsock.closed
