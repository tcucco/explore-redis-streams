from aiohttp.web import (Application, middleware)
from app.core.dependency_injector import DependencyInjectorDefinition
from app.domains.module import boot_container
from app.presentation.api.routes import setup_routes as setup_api_routes
from app.presentation.web.routes import setup_routes as setup_web_routes

def setup_app(app: Application) -> Application:
  app['container'] = boot_container(DependencyInjectorDefinition('development'))

  setup_api_routes(app)
  setup_web_routes(app)

  return app
