import os
from aiohttp.web import (Application, Request, Response)

ASSETS_ROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'assets')

def setup_routes(app: Application) -> Application:
  app.router.add_get('/', index)
  return app

def index(request: Request) -> Response:
  with open(os.path.join(ASSETS_ROOT, 'build', 'html', 'app.html')) as html:
    return Response(text=html.read(), content_type='text/html')
