'''API routes.'''
from aiohttp.web import (Application, Request, Response, WebSocketResponse)
from app.core.data import redis_connection
from app.presentation.core.websocket_interactor import WebSocketInteractor

def setup_routes(app: Application) -> Application:
  app.router.add_get('/api/get/{key}', get_value)
  app.router.add_get('/api/set/{key}', set_value)
  app.router.add_get('/ws', messaging)
  return app

async def set_value(request: Request) -> Response:
  key = request.match_info['key']
  value = request.query['value']

  async with redis_connection(request['container'].get('redis')) as conn:
    await conn.execute('SET', key, value)

  return Response(text='key {} set to {}'.format(key, value))

async def get_value(request: Request) -> Response:
  key = request.match_info['key']

  async with redis_connection(request['container'].get('redis')) as conn:
    value = await conn.execute('GET', key)

  await conn.wait_closed()

  return Response(text='key {} value is {}'.format(key, value))

async def messaging(request: Request) -> WebSocketResponse:
  '''WebSocket messageing.'''
  wsock = WebSocketResponse()
  await wsock.prepare(request)

  interaction = request['container'].get('chat_interaction')
  interactor = WebSocketInteractor(wsock)
  await interaction.execute(interactor, 100)

  return wsock
