from aiohttp.web import (Application, middleware)

@middleware
async def container_middleware(request, handler):
  request['container'] = request.app['container'].get_injector()
  return await handler(request)

def get_middleware():
  return [container_middleware]
