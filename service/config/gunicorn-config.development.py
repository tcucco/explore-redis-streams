bind = '0.0.0.0:8000'
forwarded_allow_ips = '*'
worker_class = 'aiohttp.worker.GunicornUVLoopWebWorker'
reload = True
accesslog = '-'
errorlog = '-'
