import asyncio
import os
import sys
import uvloop
from aiohttp import web

sys.path.append(os.path.abspath('..'))
from app.presentation import setup_app
from app.presentation.middleware import get_middleware

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

webapp = setup_app(web.Application(middlewares=get_middleware()))
