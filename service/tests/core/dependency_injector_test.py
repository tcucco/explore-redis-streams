# pylint: disable=missing-docstring
from unittest import TestCase
from app.core.dependency_injector import DependencyInjectorDefinition

class DependencyInjectorTest(TestCase):
  def test_services(self):
    di = DependencyInjectorDefinition('dev')

    def service_a():
      return 5

    def service_b():
      return 10

    def service_c(service_a, service_b):
      return lambda: service_a() + service_b()

    di.add('service_a', lambda di: service_a)
    di.add('service_b', lambda di: service_b)
    di.add_service('service_c', ['service_a', 'service_b'], service_c)

    inj = di.get_injector()

    srv_a = inj.get('service_a')
    srv_b = inj.get('service_b')
    srv_c = inj.get('service_c')
    
    self.assertEqual(5, srv_a())
    self.assertEqual(10, srv_b())
    self.assertEqual(15, srv_c())
