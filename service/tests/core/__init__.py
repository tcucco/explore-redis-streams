# pylint: disable=missing-docstring
__all__ = [
  'DependencyInjectorTest',
]

from .dependency_injector_test import DependencyInjectorTest
