#!/bin/bash

DIR=$(dirname $0)
TAG="redis-streams-api:latest"
DOCKER_ROOT="$DIR/../docker/development"

cd "$DOCKER_ROOT/service"

docker build . -t "$TAG"
